export type Json = string | number | boolean | null | { [key: string]: Json } | Json[];

export interface Database {
  public: {
    Tables: {
      games: {
        Row: {
          black: string | null;
          created_at: string;
          id: string;
          white: string | null;
        };
        Insert: {
          black?: string | null;
          created_at?: string;
          id?: string;
          white?: string | null;
        };
        Update: {
          black?: string | null;
          created_at?: string;
          id?: string;
          white?: string | null;
        };
        Relationships: [];
      };
    };
    Views: {
      [_ in never]: never;
    };
    Functions: {
      [_ in never]: never;
    };
    Enums: {
      [_ in never]: never;
    };
    CompositeTypes: {
      [_ in never]: never;
    };
  };
}
