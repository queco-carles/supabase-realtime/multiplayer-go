import { BoardState, Player, Position } from '@go158/go-validator';

const BOARD_SIZE = 1000;
const OFFSET = 80;

export interface BoardSettings {
  current: Ref<BoardState>;
  showCoordinates: Ref<boolean>;
  showCharacters: Ref<boolean>;
  onPositionClicked: (position: Position) => void;
}

export default ({ current, showCoordinates, showCharacters, onPositionClicked }: BoardSettings) => {
  const cellSize = computed(() => {
    return (BOARD_SIZE - OFFSET * 2) / (current.value.boardSize - 1);
  });

  const createBoard = (): SVGElement => {
    const result = document.createElementNS('http://www.w3.org/2000/svg', 'rect');

    result.setAttribute('width', `${BOARD_SIZE}`);
    result.setAttribute('height', `${BOARD_SIZE}`);
    result.setAttribute('fill', '#E48753');

    return result;
  };

  const createLines = (): Array<SVGElement> => {
    const result = Array<SVGElement>();

    for (let i = 0; i < current.value.boardSize; i += 1) {
      const horizontalTop = document.createElementNS('http://www.w3.org/2000/svg', 'line');
      horizontalTop.setAttribute('x1', `${OFFSET}`);
      horizontalTop.setAttribute('y1', `${OFFSET + i * cellSize.value}`);
      horizontalTop.setAttribute('x2', `${BOARD_SIZE - OFFSET}`);
      horizontalTop.setAttribute('y2', `${OFFSET + i * cellSize.value}`);
      horizontalTop.setAttribute('stroke-width', '1px');
      horizontalTop.setAttribute('stroke', 'black');

      const vertical = document.createElementNS('http://www.w3.org/2000/svg', 'line');
      vertical.setAttribute('x1', `${OFFSET + i * cellSize.value}`);
      vertical.setAttribute('y1', `${OFFSET}`);
      vertical.setAttribute('x2', `${OFFSET + i * cellSize.value}`);
      vertical.setAttribute('y2', `${BOARD_SIZE - OFFSET}`);
      vertical.setAttribute('stroke-width', '1px');
      vertical.setAttribute('stroke', 'black');

      result.push(horizontalTop);
      result.push(vertical);
    }

    return result;
  };

  const createHoshis = (): Array<SVGElement> => {
    const result = Array<SVGElement>();

    for (let i = 0; i < 3; i += 1) {
      for (let j = 0; j < 3; j += 1) {
        const x = OFFSET + 4 * (i + 1) * cellSize.value - cellSize.value + cellSize.value * 2 * i;
        const y = OFFSET + 4 * (j + 1) * cellSize.value - cellSize.value + cellSize.value * 2 * j;

        const circle = document.createElementNS('http://www.w3.org/2000/svg', 'circle');
        circle.setAttribute('cx', `${x}`);
        circle.setAttribute('cy', `${y}`);
        circle.setAttribute('r', '5');

        result.push(circle);
      }
    }

    return result;
  };

  const createCoordinates = (): Array<SVGElement> => {
    const result = Array<SVGElement>();

    for (let i = 0; i < current.value.boardSize; i += 1) {
      const horizontalTop = document.createElementNS('http://www.w3.org/2000/svg', 'text');
      horizontalTop.setAttribute('x', `${OFFSET + cellSize.value * i}`);
      horizontalTop.setAttribute('y', `${OFFSET / 2}`);
      horizontalTop.setAttribute('text-anchor', 'middle');
      horizontalTop.setAttribute('dominant-baseline', 'middle');
      horizontalTop.setAttribute('style', 'font: 28px sans-serif;');
      horizontalTop.innerHTML = String.fromCharCode((i >= 8 ? 66 : 65) + i);

      const horizontalBottom = document.createElementNS('http://www.w3.org/2000/svg', 'text');
      horizontalBottom.setAttribute('x', `${OFFSET + cellSize.value * i}`);
      horizontalBottom.setAttribute('y', `${BOARD_SIZE - OFFSET / 2}`);
      horizontalBottom.setAttribute('text-anchor', 'middle');
      horizontalBottom.setAttribute('dominant-baseline', 'middle');
      horizontalBottom.setAttribute('style', 'font: 28px sans-serif;');
      horizontalBottom.innerHTML = String.fromCharCode((i >= 8 ? 66 : 65) + i);

      const verticalLeft = document.createElementNS('http://www.w3.org/2000/svg', 'text');
      verticalLeft.setAttribute('x', `${OFFSET / 2}`);
      verticalLeft.setAttribute('y', `${OFFSET + cellSize.value * i}`);
      verticalLeft.setAttribute('text-anchor', 'middle');
      verticalLeft.setAttribute('dominant-baseline', 'middle');
      verticalLeft.setAttribute('style', 'font: 28px sans-serif;');
      verticalLeft.innerHTML = `${current.value.boardSize - i}`;

      const verticalRight = document.createElementNS('http://www.w3.org/2000/svg', 'text');
      verticalRight.setAttribute('x', `${BOARD_SIZE - OFFSET / 2}`);
      verticalRight.setAttribute('y', `${OFFSET + cellSize.value * i}`);
      verticalRight.setAttribute('text-anchor', 'middle');
      verticalRight.setAttribute('dominant-baseline', 'middle');
      verticalRight.setAttribute('style', 'font: 28px sans-serif;');
      verticalRight.innerHTML = `${current.value.boardSize - i}`;

      result.push(horizontalTop);
      result.push(horizontalBottom);
      result.push(verticalLeft);
      result.push(verticalRight);
    }

    return result;
  };

  const createCharacters = (): Array<SVGElement> => {
    const result = new Array<SVGElement>();

    current.value.characters.forEach((row, i) => {
      row.forEach((character, j) => {
        if (character === null) return;

        const group = document.createElementNS('http://www.w3.org/2000/svg', 'g');

        const circle = document.createElementNS('http://www.w3.org/2000/svg', 'circle');
        circle.setAttribute('cx', `${OFFSET + j * cellSize.value}`);
        circle.setAttribute('cy', `${OFFSET + i * cellSize.value}`);
        circle.setAttribute('r', `${cellSize.value / 3}`);
        circle.setAttribute('fill', '#E48753');
        group.appendChild(circle);

        const text = document.createElementNS('http://www.w3.org/2000/svg', 'text');
        text.setAttribute('x', `${OFFSET + j * cellSize.value}`);
        text.setAttribute('y', `${OFFSET + i * cellSize.value}`);
        text.setAttribute('text-anchor', 'middle');
        text.setAttribute('dominant-baseline', 'middle');
        text.setAttribute('style', `font: ${character.length > 2 ? '20' : '25'}px sans-serif; cursor: default`);
        text.innerHTML = character;
        group.appendChild(text);

        result.push(group);
      });
    });

    return result;
  };

  const createStones = (): Array<SVGElement> => {
    return current.value.players
      .map((row, i) => {
        return row.map((player, j) => {
          if (player === null) {
            return undefined;
          }

          const circle = document.createElementNS('http://www.w3.org/2000/svg', 'circle');

          circle.setAttribute('cx', `${OFFSET + j * cellSize.value}`);
          circle.setAttribute('cy', `${OFFSET + i * cellSize.value}`);
          circle.setAttribute('fill', player === Player.BLACK ? 'black' : 'white');
          circle.setAttribute('r', '21');
          circle.setAttribute('filter', 'url(#shadow)');

          return circle;
        });
      })
      .flat()
      .filter((value) => value !== undefined) as Array<SVGElement>;
  };

  const createLastPositionIndicator = (): SVGElement | undefined => {
    const { lastPosition, playerToMove } = current.value;
    if (lastPosition === null || lastPosition === undefined) {
      return;
    }

    const circle = document.createElementNS('http://www.w3.org/2000/svg', 'circle');

    circle.setAttribute('cx', `${OFFSET + lastPosition.col * cellSize.value}`);
    circle.setAttribute('cy', `${OFFSET + lastPosition.row * cellSize.value}`);
    circle.setAttribute('stroke', playerToMove === Player.BLACK ? 'black' : 'white');
    circle.setAttribute('fill', 'none');
    circle.setAttribute('r', '12');
    circle.setAttribute('stroke-width', '3');

    return circle;
  };

  const createShadowFilter = (): SVGElement => {
    const filter = document.createElementNS('http://www.w3.org/2000/svg', 'filter');

    const feDropShadow = document.createElementNS('http://www.w3.org/2000/svg', 'feDropShadow');

    feDropShadow.setAttribute('dx', '2');
    feDropShadow.setAttribute('dy', '2');
    feDropShadow.setAttribute('stdDeviation', '3');
    feDropShadow.setAttribute('flood-opacity', '0.5');

    filter.setAttribute('id', 'shadow');
    filter.setAttribute('width', '180%');
    filter.setAttribute('height', '180%');
    filter.setAttribute('x', '-40%');
    filter.setAttribute('y', '-40%');
    filter.appendChild(feDropShadow);

    return filter;
  };

  const svg = computed(() => {
    const result = document.createElementNS('http://www.w3.org/2000/svg', 'svg');

    result.setAttribute('viewBox', `0 0 ${BOARD_SIZE} ${BOARD_SIZE}`);

    result.setAttribute('width', '100%');

    result.setAttribute('height', '100%');

    const filter = createShadowFilter();
    result.appendChild(filter);

    const board = createBoard();
    result.appendChild(board);

    const lines = createLines();
    result.append(...lines);

    const hoshis = createHoshis();
    result.append(...hoshis);

    if (showCoordinates.value) {
      const coordinates = createCoordinates();
      result.append(...coordinates);
    }

    const stones = createStones();
    result.append(...stones);

    if (showCharacters.value) {
      const characters = createCharacters();
      result.append(...characters);
    }

    const lastPositionInicator = createLastPositionIndicator();

    if (lastPositionInicator !== undefined) {
      result.append(lastPositionInicator);
    }

    return result;
  });

  const { x, y } = useMouse({
    type: 'client',
  });

  const mousePosition = computed(() => {
    const point = svg.value.createSVGPoint();
    point.x = x.value;
    point.y = y.value;

    const { x: boardX, y: boardY } = point.matrixTransform(svg.value.getScreenCTM()?.inverse());

    const row = Math.round((boardY - OFFSET) / cellSize.value);
    const col = Math.round((boardX - OFFSET) / cellSize.value);

    const topMargin = boardY - OFFSET - (row * cellSize.value - cellSize.value / 2);
    const leftMargin = boardX - OFFSET - (col * cellSize.value - cellSize.value / 2);

    const minMargin = Math.min(leftMargin, topMargin, cellSize.value - leftMargin, cellSize.value - topMargin);

    // Avoid missclicks
    if (minMargin < 0.1 * cellSize.value) {
      return null;
    }

    if (row < 0 || row > current.value.boardSize - 1 || col < 0 || col > current.value.boardSize - 1) {
      return null;
    }

    return {
      row,
      col,
    };
  });

  useEventListener(svg, 'click', () => {
    const position = mousePosition.value;

    if (position === null) return;

    onPositionClicked(position);
  });

  return {
    svg,
  };
};
