// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: { enabled: true },
  modules: ['nuxt-icon', '@vueuse/nuxt', '@nuxtjs/supabase', '@nuxtjs/tailwindcss', '@nuxtjs/eslint-module'],
  css: ['assets/css/main.css'],
  eslint: { lintOnStart: false },
});
