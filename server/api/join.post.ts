import { Player } from '@go158/go-validator';
import { Database } from '../../types/database';
import { serverSupabaseServiceRole } from '#supabase/server';

export default defineEventHandler(async (event) => {
  const body = await readBody<{ id: string }>(event);
  const supabase = serverSupabaseServiceRole<Database>(event);
  const session = await useSession(event, { password: process.env.SESSION_PASSWORD! });

  if (body?.id === undefined) throw createError({ message: 'Game id not provided', statusCode: 400 });

  const { data } = await supabase.from('games').select('*').eq('id', body.id).single();

  if (data === null) throw createError({ statusCode: 404, message: 'Game does not exist' });

  if (data.black === session.id) return { player: Player.BLACK };
  if (data.white === session.id) return { player: Player.WHITE };

  if (!data.black && !data.white) {
    const isBlack = Math.random() < 0.5;

    if (isBlack) await supabase.from('games').update({ black: session.id }).eq('id', body.id);
    else await supabase.from('games').update({ white: session.id }).eq('id', body.id);

    return { player: isBlack ? Player.BLACK : Player.WHITE };
  }

  if (!data.black) {
    await supabase.from('games').update({ black: session.id }).eq('id', body.id);
    return { player: Player.BLACK };
  }

  if (!data.white) {
    await supabase.from('games').update({ white: session.id }).eq('id', body.id);
    return { player: Player.WHITE };
  }

  throw createError({ statusCode: 409, message: 'Game is full' });
});
