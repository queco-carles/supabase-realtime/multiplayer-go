import { Database } from '../../types/database';
import { serverSupabaseServiceRole } from '#supabase/server';

export default defineEventHandler(async (event) => {
  const supabase = serverSupabaseServiceRole<Database>(event);
  const { data } = await supabase.from('games').insert({}).select().single();
  return { result: { id: data?.id } };
});
