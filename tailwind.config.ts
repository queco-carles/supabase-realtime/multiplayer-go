import type { Config } from 'tailwindcss';

export default <Partial<Config>>{
  theme: {
    extend: {
      maxWidth: {
        main: '1280px',
      },
    },
  },
};
