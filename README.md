# Go multiplayer - a Supabase Realtime example

This project leverages the power and flexibility of Supabase Realtime by providing a multiplayer experience of playing the ancient game of Go with other players.

> Note: Right now, the Go library used doesn't have the functionality for finishing a Game. This project only tries to showcase how Supabase Realtime can be used to implement a one-to-one multiplayer game like Go or Chess.

## Features

- 🎲 Start a new game
- 🔗 Join an existing game
- 🍪 Game id is stored in session so it is not lost when the window is closed
- 🫙 Game record is stored in session storage

## Development

To start developing, you will need Supabase cli tool. You can install it from [here](https://supabase.com/docs/guides/cli).

1. Install dependencies with `pnpm install`

2. Start the supabase local instance by running `supabase start`

3. Copy the .env.example file to .env and fill it with the following variables:

   - `SUPABSE_URL`: you can get this value from `supabase status`
   - `SUPABSE_KEY`: you can get this value from `supabase status`
   - `SUPABSE_SERVICE_KEY`: you can get this value from `supabase status`
   - `SESSION_PASSWORD`: Whatever password you want to use to encrypt the session cookies. Must be >24 characters

4. Start the development environment by running `pnpm dev`
